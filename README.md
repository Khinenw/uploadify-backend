# Uploadify Backend
> A backend for uploadify

## Environments
* `AWS_ACCESS_KEY_ID`: The access key id for your AWS credentials
* `AWS_SECRET_ACCESS_KEY`: The secret access key for your AWS credentials
* `AWS_S3_BUCKET`: Name of your bucket, which will be used to save the files
* `AWS_S3_REGION`: Region of your bucket (example: ap-northeast-2)
* `DB_NAME`: Name for the DB
* `DB_USERNAME`: Username for the DB
* `DB_PASSWORD`: Password for the DB
* `DB_HOST`: Host for the DB

## Tools
* `node bin/createUser.js`: The tool to make an user

