const createAsyncRouter = require('@khinenw/express-async-router');
const crypto = require('crypto');
const genPassword = require('../utils/genPassword');
const jwt = require('jsonwebtoken');
const secret = require('../utils/secret');

const User = require('../models/User');

const router = createAsyncRouter();
router.post('/login', async (req, res) => {
	const { username, password } = req.body;
	if (
		typeof username !== 'string' ||
		typeof password !== 'string' ||
		!/[a-zA-Z0-9]{4,64}/.test(username)
	) {
		res.status(422).json({
			ok: false,
			reason: 'no-username-or-password'
		});

		return;
	}

	const user = await User.findOne({
		where: { username }
	});

	if (!user) {
		res.status(403).json({
			ok: false,
			reason: 'no-such-user-or-wrong-password'
		});
		return;
	}

	const [ savedSalt, savedPassword ] = user.password.split('$');
	const hash = await genPassword(password, savedSalt);

	if (!hash || hash !== savedPassword) {
		res.status(403).json({
			ok: false,
			reason: 'no-such-user-or-wrong-password'
		});
		return;
	}

	const payload = { username, type: 'auth' };
	const token = await new Promise(resolve => {
		jwt.sign(payload, secret, { expiresIn: '6h' }, (err, token) => {
			if (err) {
				resolve(null);
				return;
			}

			resolve(token);
		});
	});

	res.json({
		ok: true,
		token
	});
});

module.exports = router;
