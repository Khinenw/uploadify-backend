const createAsyncRouter = require('@khinenw/express-async-router');
const jwt = require('jsonwebtoken');
const s3 = require('../utils/s3');
const secret = require('../utils/secret');

const File = require('../models/File');

const router = createAsyncRouter();

router.get('/:name(\\d+)', async (req, res) => {
	const token = req.query.token;

	if (typeof token !== 'string') {
		res.status(403).send("No token given");
		return;
	}

	const name = parseInt(req.params.name);

	const decoded = await new Promise(resolve => {
		jwt.verify(token, secret, (err, decoded) => {
			if (err) {
				resolve(null);
				return;
			}

			resolve(decoded);
		});
	});

	if (!decoded || decoded.type !== 'download' || decoded.target !== name) {
		res.status(403).send("Wrong token given");
		return;
	}

	const file = await File.findOne({
		where: { id: name }
	});

	s3.getObject({ Bucket: process.env.AWS_S3_BUCKET, Key: file.s3Key })
		.on('httpHeaders', function (status, headers) {
			if (res.headersSent) {
				// This might not happen
				return;
			}

			res.set('Content-Length', headers['content-length']);
			res.set('Content-Type', headers['content-type']);
			res.set('Last-Modified', headers['last-modified']);
			res.attachment(file.name);
		})
		.createReadStream()
		.on('error', () => next(new Error("S3 Read Error")))
		.pipe(res);
});

module.exports = router;
