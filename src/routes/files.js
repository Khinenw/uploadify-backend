const authNeeded = require('../middlewares/authNeeded');
const createAsyncRouter = require('@khinenw/express-async-router');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const multerS3 = require('multer-s3');
const s3 = require('../utils/s3');
const secret = require('../utils/secret');
const sequelize = require('../utils/sequelize');

const File = require('../models/File');
const User = require('../models/User');

const router = createAsyncRouter();
router.get('/list', authNeeded, async (req, res) => {
	const files = await File.findAll({
		attributes: [ 'id', 'name', 'author' ]
	});

	res.json({
		ok: true,
		files
	});
});

router.get('/download/:name(\\d+)', authNeeded, async (req, res) => {
	const payload = { type: 'download', target: parseInt(req.params.name) };
	const downloadToken = await new Promise(resolve => {
		jwt.sign(payload, secret, { expiresIn: '6h' }, (err, token) => {
			if (err) {
				resolve(null);
				return;
			}

			resolve(token);
		});
	});

	res.json({
		ok: true,
		token: downloadToken
	});
});

const upload = multer({
	storage: multerS3({
		s3,
		bucket: process.env.AWS_S3_BUCKET,
		contentDisposition: 'attachment',
		key: (req, file, cb) => cb(null, Date.now().toString())
	})
});

router.post('/upload', authNeeded, upload.single('file'), async (req, res) => {
	const name = req.file.originalname || 'Unnamed File';
	const transaction = await sequelize.transaction();

	const existingFile = await File.findOne({
		where: { name },
		transaction
	});

	if (existingFile) {
		await transaction.rollback();
		res.json({
			ok: false,
			reason: 'file-already-exists'
		});
		return;
	}

	const file = await File.create({
		name,
		s3Key: req.file.key,
		author: req.user.username
	}, { transaction });

	await transaction.commit();

	res.json({
		ok: true,
		file: { id: file.id, name: file.name, author: file.author }
	});
});

module.exports = router;
