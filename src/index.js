require('dotenv').config();

const bodyParser = require('body-parser');
const express = require('express');
const jwt = require('jsonwebtoken');
const sequelize = require('./utils/sequelize');

const auth = require('./routes/auth');
const files = require('./routes/files');
const downloads = require('./routes/downloads');

const app = express();
app.use(bodyParser.json());

app.get('/', (req, res) => {
	res.status(418).json({
		ok: true,
		me: 'teapot'
	});
});

app.use('/api/auth', auth);
app.use('/api/files', files);
app.use('/downloads', downloads);

app.use((err, req, res, next) => {
	console.log(err);
	if (!res.headersSent) {
		res.status(500).send('500ISE T_T');
	}
});

(async () => {
	await sequelize.sync();

	const port = parseInt(process.env.PORT || '3008');
	app.listen(port);
	console.log("Listening!");
})();
