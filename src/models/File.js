const { Model, DataTypes } = require('sequelize');
const User = require('./User');

const sequelize = require('../utils/sequelize');

class File extends Model {}
File.init({
	id: {
		type: DataTypes.INTEGER,
		autoIncrement: true,
		primaryKey: true
	},

	name: {
		type: DataTypes.STRING(2048),
		allowNull: false
	},

	s3Key: {
		type: DataTypes.STRING(2048),
		allowNull: false
	}
}, { sequelize });
User.hasMany(File, { foreignKey: 'author' });
File.belongsTo(User, { foreignKey: 'author' });

module.exports = File;
