const { Model, DataTypes } = require('sequelize');
const sequelize = require('../utils/sequelize');

class User extends Model {}
User.init({
	username: {
		type: DataTypes.STRING,
		primaryKey: true
	},

	password: {
		type: DataTypes.STRING(1536),
		allowNull: false
	}
}, { sequelize });

module.exports = User;
