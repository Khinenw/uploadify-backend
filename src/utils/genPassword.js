const crypto = require('crypto');

module.exports = async (password, salt) => {
	const buff = Buffer.from(salt, 'base64');

	return new Promise(resolve => crypto.pbkdf2(
		password, salt, 65535, 512, 'sha512',
		(err, derivedKey) => {
			if (err) {
				resolve(null);
				return;
			}

			resolve(derivedKey.toString('base64'));
		}
	));
};
