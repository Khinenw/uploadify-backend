const secret = require('../utils/secret');
const jwt = require('jsonwebtoken');

const User = require('../models/User');

async function auth(req, res, next) {
	const token = req.get('Authorization');
	if (!token) {
		next();
		return;
	}

	const decoded = await new Promise(resolve => {
		jwt.verify(token, secret, (err, decoded) => {
			if (err) {
				resolve(null);
				return;
			}

			resolve(decoded);
		});
	});

	if (decoded && decoded.type === 'auth') {
		req.user = decoded;
	}

	next();
}

module.exports = auth;
