const auth = require('./auth');

function authNeeded(req, res, next) {
	auth(req, res, err => {
		if (err) {
			next(err);
			return;
		}
		
		if (req.user) {
			next();
			return;
		}
		
		res.status(403).json({
			ok: false,
			reason: 'not-logged-in'
		});
	});
}

module.exports = authNeeded;
