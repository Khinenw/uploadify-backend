#!/usr/bin/env node
require('dotenv').config();

const crypto = require('crypto');
const genPassword = require('../src/utils/genPassword');
const readline = require('readline');
const sequelize = require('../src/utils/sequelize');

const User = require('../src/models/User');

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

console.log("Ctrl + C to stop!");

sequelize.sync().then(() => {
	console.log("\n===== Creating New User =====");
	rl.question('Please enter username: ', async username => {
		username = username.trim();

		if (!/^[a-zA-Z0-9]{4,64}/.test(username)) {
			console.log("Wrong username! Username should be: alphabet, numbers, 4 ~ 64 chars");
			rl.close();
			return;
		}

		rl.question('Please enter password: ', async password => {
			password = password.trim();

			const random = await new Promise((resolve) => {
				crypto.randomBytes(256, (err, buff) => {
					if (err) {
						resolve(null);
						return;
					}

					resolve(buff);
				});
			});

			if (!random)
				return null;

			const salt = random.toString('base64');
			const finalized = await genPassword(password, salt);

			console.log();
			console.log("Username/Password you have entered:");
			console.log(username + " / " + password);
			console.log();


			rl.question('Is it right? (y/n)', async answer => {
				if (answer.trim() === 'y') {
					await User.create({
						username,
						password: `${salt}$${finalized}`
					});

					console.log("Created");
				} else {
					console.log("Cancelled.");
				}

				rl.close();
			});
		});
	});
});
